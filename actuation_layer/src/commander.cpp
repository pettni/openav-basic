// Implementation of ActuationLayerCommander class. 
//
// Authored by Petter Nilsson (pettni@umich.edu), November 2016

#include <PolySyncCore.hpp>

#include "definitions_actuation_layer.hpp"

#include "commander.hpp"


void ActuationLayerCommander::activate(polysync::Node & node_ref)
{
  // Requires a valid polysync::Node & AFTER connectPolySync()
  brake_message =  new PlatformBrakeCommandMessage(node_ref);
  steering_message = new PlatformSteeringCommandMessage(node_ref);
  throttle_message =  new PlatformThrottleCommandMessage(node_ref);
  gear_message =  new PlatformGearCommandMessage(node_ref);
  parameters_message =  new ParametersMessage(node_ref);

  set_all_safe();
  set_actuation_mode(0);

  activated = 1;
}


void ActuationLayerCommander::set_brake_safe()
{
  if (!activated) {
    polySyncLogError("ActuationLayerCommander not initialized");
    return;
  }
  brake_message->setBrakeCommandType(BRAKE_COMMAND_INVALID);
  brake_message->setEnabled(0);
  brake_message->setBooEnabled(0);
  brake_message->setBrakeCommand(0.0);
}


void ActuationLayerCommander::set_steering_safe()
{
  if (!activated) {
    polySyncLogError("ActuationLayerCommander not initialized");
    return;
  }
  steering_message->setSteeringCommandKind(STEERING_COMMAND_INVALID);
  steering_message->setEnabled(0);
  steering_message->setSteeringWheelAngle(0.0);
}


void ActuationLayerCommander::set_throttle_safe()
{
  if (!activated) {
    polySyncLogError("ActuationLayerCommander not initialized");
    return;
  }
  throttle_message->setThrottleCommandType(THROTTLE_COMMAND_INVALID);
  throttle_message->setEnabled(0);
  throttle_message->setThrottleCommand(0.0);
}


void ActuationLayerCommander::set_all_safe()
{
  // Reset all messages to neutral values
  if (!activated) {
    polySyncLogError("ActuationLayerCommander not initialized");
    return;
  }
  set_brake_safe();
  set_steering_safe();
  set_throttle_safe();
  gear_message->setGearPosition(GEAR_POSITION_INVALID);
}    


void ActuationLayerCommander::set_actuation_mode(bool enabled)
{
  // Send message to dataspeed interface to toggle
  // actuation
  if (!activated) {
    polySyncLogError("ActuationLayerCommander not initialized");
    return;
  }

  if (!enabled)
  { 
    set_all_safe();
  }

  if (parameters_message)
  {
    // Parameter value
    polysync::datamodel::ParameterValue parameter_value;
    parameter_value.setParameterValueKind( PARAMETER_VALUE_ULONGLONG );
    parameter_value.setUllValue( enabled );

    // Parameter
    polysync::datamodel::Parameter parameter;
    parameter.setId( UM_PARAM_ACTUATION );
    parameter.setValue(parameter_value);

    // Parameter vector
    std::vector< polysync::datamodel::Parameter > parameter_vector;
    parameter_vector.push_back(parameter);

    // Parameters message
    parameters_message->setDestGuid(0xFFFFFFFF00000000);  // all
    parameters_message->setType(PARAMETER_MESSAGE_SET_VALUE);
    parameters_message->setParameters(parameter_vector);
    parameters_message->setHeaderTimestamp(polysync::getTimestamp());

    parameters_message->publish();
  }
}


void ActuationLayerCommander::request_actuation_status()
{
  if (!activated) {
    polySyncLogError("ActuationLayerCommander not initialized");
    return;
  }
  // Request actuation status from dataspeed interface
  polysync::datamodel::Parameter parameter;
  parameter.setId( UM_PARAM_ACTUATION );

  // Parameter vector
  std::vector< polysync::datamodel::Parameter > parameter_vector;
  parameter_vector.push_back(parameter);

  // Parameters message
  parameters_message->setDestGuid(0xFFFFFFFF00000000);  // all
  parameters_message->setType(PARAMETER_MESSAGE_GET_VALUE);
  parameters_message->setParameters(parameter_vector);
  parameters_message->setHeaderTimestamp(polysync::getTimestamp());

  parameters_message->publish();
}


void ActuationLayerCommander::assure_gear_safe()
{
  // Don't shift if car is moving
  float speed = ( vehicle_state.wheel_speed.getFrontLeft()
                 + vehicle_state.wheel_speed.getFrontRight()
                 + vehicle_state.wheel_speed.getRearLeft()
                 + vehicle_state.wheel_speed.getRearRight() ) / 4;
  if (gear_message->getGearPosition() != GEAR_POSITION_INVALID
      && abs(speed) > 0.)
  {
    polySyncLogWarning("Gear command override: car is moving");
    gear_message->setGearPosition(GEAR_POSITION_INVALID);
  }
}


void ActuationLayerCommander::assure_throttle_safe()
{
  // Don't actuate throttle if braking is active
  // or gear not in DRIVE
  if (brake_message->getBrakeCommand() > 0 
      && throttle_message->getThrottleCommand() > 0)
  {
    polySyncLogWarning("Throttle command override: brakes applied");
    throttle_message->setThrottleCommand(0);
  }

  if (vehicle_state.gear.getPosition() != GEAR_POSITION_DRIVE 
      && throttle_message->getThrottleCommand() > 0)
  {
    polySyncLogWarning("Throttle command override: not in DRIVE");
    throttle_message->setThrottleCommand(0);
  }
}


void ActuationLayerCommander::constrain_all()
{  
  if (!activated) {
    polySyncLogError("ActuationLayerCommander not initialized");
    return;
  }
  // Constrain braking to [0, BRAKE_MAX_]
  if (brake_message->getBrakeCommand() < 0.)
  {
    brake_message->setBrakeCommand(0.);
  }
  else if (brake_message->getBrakeCommand() > BRAKE_MAX)
  {
    brake_message->setBrakeCommand(BRAKE_MAX);
  }

  // Constrain throttle to [0, THROTTLE_MAX]
  if (throttle_message->getThrottleCommand() < 0.)
  {
    throttle_message->setThrottleCommand(0.);
  }
  else if (throttle_message->getThrottleCommand() > THROTTLE_MAX)
  {
    throttle_message->setThrottleCommand(THROTTLE_MAX);
  }

  // Constrain steering to [-STEERING_MAX, STEERING_MAX]
  if (steering_message->getSteeringWheelAngle() < -STEERING_MAX)
  {
    steering_message->setSteeringWheelAngle(-STEERING_MAX);
  }
  else if (steering_message->getSteeringWheelAngle() > -STEERING_MAX)
  {
    steering_message->setSteeringWheelAngle(-STEERING_MAX);
  }
}


void ActuationLayerCommander::check_and_publish_all()
{
  if (!activated) {
    polySyncLogError("ActuationLayerCommander not initialized");
    return;
  }

  assure_throttle_safe();   // disable throttle if brakes applied
  assure_gear_safe();       // disable shifting if car moving
  constrain_all();          // constrain commands to predefined intervals

  int now = polysync::getTimestamp();

  brake_message->setHeaderTimestamp(now); 
  brake_message->publish();

  throttle_message->setHeaderTimestamp(now);
  throttle_message->publish();

  steering_message->setHeaderTimestamp(now);
  steering_message->publish();

  if (gear_message->getGearPosition() != GEAR_POSITION_INVALID) {
    gear_message->setHeaderTimestamp(now);
    gear_message->publish();
  }

}