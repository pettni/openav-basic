// Implementation of ActuationLayerNode 
//
// Authored by Petter Nilsson (pettni@umich.edu), November 2016

#include <iostream>

#include <PolySyncCore.hpp>
#include <PolySyncNode.hpp>
#include <PolySyncDataModel.hpp>

#include "definitions_actuation_layer.hpp"

#include "node.hpp"

using namespace std;

void ActuationLayerNode::messageEvent( std::shared_ptr< polysync::Message > message )
{
  using namespace polysync::datamodel;

  // Check if a direct actuation command was published from elsewhere,
  // if so, disable actuation
  if ( (
        getSubclass< PlatformThrottleCommandMessage >( message ) 
        || getSubclass< PlatformBrakeCommandMessage >( message )
        || getSubclass< PlatformSteeringCommandMessage >( message )
        || getSubclass< PlatformGearCommandMessage >( message )
       ) && message->getSourceGuid() != getGuid() )
  {   
      commander->set_actuation_mode(0);
      polySyncLogError("Direct actuation command detected, actuation disabled");
      return;
  }

  // Copy UM messages to commander interface as received
  if( auto throttle_message = getSubclass< UmPlatformThrottleCommandReqMsg >( message ) )
  {
    commander->throttle_message->setThrottleCommandType(
      throttle_message->getThrottleCommandType());
    commander->throttle_message->setThrottleCommand(
      throttle_message->getThrottleCommand());
  }

  if( auto brake_message = getSubclass< UmPlatformBrakeCommandReqMsg >( message ) )
  {
    commander->brake_message->setBrakeCommandType(
      brake_message->getBrakeCommandType());
    commander->brake_message->setBrakeCommand(
      brake_message->getBrakeCommand());
  }

  if( auto steering_message = getSubclass< UmPlatformSteeringCommandReqMsg >( message ) )
  {
    commander->steering_message->setSteeringCommandKind(
      steering_message->getSteeringCommandKind());
    commander->steering_message->setSteeringWheelAngle(
      steering_message->getSteeringWheelAngle());
    commander->steering_message->setMaxSteeringWheelRotationRate(
      steering_message->getMaxSteeringWheelRotationRate());
  }


  // Check if we got updated parameter as respond to request
  if ( std::shared_ptr<ParametersMessage> parameters_message =
   getSubclass<ParametersMessage>(message) ) 
  {   
    if (parameters_message->getDestGuid() != getGuid()
        || parameters_message->getType() != PARAMETER_MESSAGE_RESPONSE)
    {
      return;
    }

    for (Parameter parameter : parameters_message->getParameters())
    {
      if (parameter.getId() == UM_PARAM_ACTUATION)
      {
        emit actuation_status(parameter.getValue().getUllValue());
      }
    }
  }


  // Copy report messages to internal state
  if ( std::shared_ptr<PlatformBrakeReportMessage> brake_msg =
   getSubclass<PlatformBrakeReportMessage>(message) ) 
  { commander->vehicle_state.brake = *brake_msg; }

  if ( std::shared_ptr<PlatformThrottleReportMessage> th_msg =
   getSubclass<PlatformThrottleReportMessage>(message) ) 
  { commander->vehicle_state.throttle = *th_msg; }

  if ( std::shared_ptr<PlatformSteeringReportMessage> st_msg =
   getSubclass<PlatformSteeringReportMessage>(message) ) 
  { commander->vehicle_state.steering = *st_msg; }

  if ( std::shared_ptr<PlatformGearReportMessage> gear_rep_msg =
   getSubclass<PlatformGearReportMessage>(message) ) 
  { commander->vehicle_state.gear = *gear_rep_msg; }

  if ( std::shared_ptr<PlatformSuspensionReportMessage> susp_rep_msg =
   getSubclass<PlatformSuspensionReportMessage>(message) ) 
  { commander->vehicle_state.suspension = *susp_rep_msg; }

  if ( std::shared_ptr<PlatformTirePressureReportMessage> tire_pressure_rep_msg =
   getSubclass<PlatformTirePressureReportMessage>(message) ) 
  { commander->vehicle_state.tire_pressure = *tire_pressure_rep_msg; }

  if ( std::shared_ptr<PlatformWheelSpeedReportMessage> ws_rep_msg =
   getSubclass<PlatformWheelSpeedReportMessage>(message) ) 
  { commander->vehicle_state.wheel_speed = *ws_rep_msg; }

  if ( std::shared_ptr<GpsMessage> gps_msg =
   getSubclass<GpsMessage>(message) ) 
  { commander->vehicle_state.gps = *gps_msg; }

}

void ActuationLayerNode::initStateEvent()
{
  commander->activate(*this);

  // Register to UM messages
  registerListener( getMessageTypeByName( "um_platform_throttle_command_req_msg" ) );
  registerListener( getMessageTypeByName( "um_platform_steering_command_req_msg" ) );
  registerListener( getMessageTypeByName( "um_platform_gear_command_req_msg" ) );
  registerListener( getMessageTypeByName( "um_platform_brake_command_req_msg" ) );

  // Register to actuation messages
  registerListener( getMessageTypeByName( "ps_platform_throttle_command_msg" ) );
  registerListener( getMessageTypeByName( "ps_platform_steering_command_msg" ) );
  registerListener( getMessageTypeByName( "ps_platform_gear_command_msg" ) );
  registerListener( getMessageTypeByName( "ps_platform_brake_command_msg" ) );

  // Parameters message
  registerListener( getMessageTypeByName( "ps_parameters_msg" ) );

  // Report messages
  registerListener( getMessageTypeByName( "ps_platform_brake_report_msg" ) );
  registerListener( getMessageTypeByName( "ps_platform_throttle_report_msg" ) );
  registerListener( getMessageTypeByName( "ps_platform_steering_report_msg" ) );
  registerListener( getMessageTypeByName( "ps_platform_gear_report_msg" ) );
  registerListener( getMessageTypeByName( "ps_platform_suspension_report_msg" ) );
  registerListener( getMessageTypeByName( "ps_platform_tire_pressure_report_msg" ) );
  registerListener( getMessageTypeByName( "ps_platform_wheel_speed_report_msg" ) );
  
  registerListener( getMessageTypeByName( "ps_gps_msg" ) );


}

void ActuationLayerNode::okStateEvent()
{
  // called continuously while in OK state
  try
  {
    commander->check_and_publish_all();
  }
  catch (polysync::DTCException e)
  {
    polySyncLogDebug( "commander->publish_all - " + e.getDtcDescription() );
    activateFault( e.getDtc(), NODE_STATE_FATAL );
  }

  if (status_counter == STATUS_RATE){
    commander->request_actuation_status();
    emit psync_status(0, getGuid());
    status_counter = 0;
  }

  polysync::sleepMicro(SAMPLE_TIME);
  ++status_counter;
}

void ActuationLayerNode::fatalStateEvent()
{
  // called once upon entering FATAL state
  emit psync_status(1, 0);
}

void ActuationLayerNode::errorStateEvent()
{
  // called continuously while in ERROR state
  emit psync_status(2,0);
}

void ActuationLayerNode::warnStateEvent()
{
  // called continuously while in WARN state
  emit psync_status(3,0);
}

void ActuationLayerNode::releaseStateEvent()
{
  // called at end of node life
  emit psync_status(4,0);
}
