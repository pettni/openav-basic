// Implementation of ActuationLayerWindow
// 
// Authored by Petter Nilsson (pettni@umich.edu), November 2016

#include "window.hpp"
#include "ui_window.h"

ActuationLayerWindow::ActuationLayerWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ActuationLayerWindow)
{
    ui->setupUi(this);
    show();
}

ActuationLayerWindow::~ActuationLayerWindow()
{
    delete ui;
}