// Middle layer that intercepts all actuation commands 
// and inspects them before passing them on.
//
// Authored by Petter Nilsson (pettni@umich.edu), November 2016

#include <QApplication>
#include <QThread>
#include <QTimer>
#include <QCheckBox>

#include "node.hpp"
#include "commander.hpp"
#include "window.hpp"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // Polysync side
    auto commander =
            std::shared_ptr< ActuationLayerCommander >( new ActuationLayerCommander );
    auto actuationLayer =
            std::unique_ptr< ActuationLayerNode >( new ActuationLayerNode(commander) );

    QThread * ps_thread = new QThread;
    actuationLayer->moveToThread( ps_thread );
    ps_thread->start();

    QThread * cmd_thread = new QThread;
    commander->moveToThread(cmd_thread);
    cmd_thread->start();

    // GUI side
    auto actuationLayerWindow = 
            std::unique_ptr<ActuationLayerWindow>(new ActuationLayerWindow);
    
    // Connections
    QObject::connect(actuationLayerWindow.get(),
            &ActuationLayerWindow::s_actuation_mode,
            commander.get(),
            &ActuationLayerCommander::actuation_mode);
    QObject::connect(actuationLayerWindow.get(),
            &ActuationLayerWindow::s_steering_mode,
            commander.get(),
            &ActuationLayerCommander::steering_mode);
    QObject::connect(actuationLayerWindow.get(),
            &ActuationLayerWindow::s_braking_mode,
            commander.get(),
            &ActuationLayerCommander::braking_mode);
    QObject::connect(actuationLayerWindow.get(),
            &ActuationLayerWindow::s_throttle_mode,
            commander.get(),
            &ActuationLayerCommander::throttle_mode);

    QObject::connect(actuationLayer.get(),
            &ActuationLayerNode::psync_status,
            actuationLayerWindow.get(),
            &ActuationLayerWindow::psync_status);
    QObject::connect(actuationLayer.get(),
            &ActuationLayerNode::actuation_status,
            actuationLayerWindow.get(),
            &ActuationLayerWindow::actuation_status);

    // Start Polysync loop
    QTimer::singleShot(50, actuationLayer.get(), SLOT(slot_run()) );

    // Run until GUI is closed
    int appReturn = a.exec();

    cmd_thread->requestInterruption();
    cmd_thread->deleteLater();

    ps_thread->requestInterruption();
    ps_thread->deleteLater();

    return appReturn;
}
