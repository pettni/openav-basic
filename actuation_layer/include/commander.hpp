// Header for ActuationLayerCommander class
// 
// Authored by Petter Nilsson (pettni@umich.edu), November 2016

#include <PolySyncNode.hpp>
#include <PolySyncDataModel.hpp>

#include <QObject>

#include "um_general.hpp"

#ifndef UM_COMMANDER_H
#define UM_COMMANDER_H

using namespace polysync::datamodel;


class ActuationLayerCommander : public QObject {

  Q_OBJECT

public:

  struct VehicleState_
  {
    PlatformBrakeReportMessage brake;
    PlatformThrottleReportMessage throttle;
    PlatformSteeringReportMessage steering;
    PlatformGearReportMessage gear;
    PlatformSuspensionReportMessage suspension;
    PlatformTirePressureReportMessage tire_pressure;
    PlatformWheelSpeedReportMessage wheel_speed;

    GpsMessage gps;
  };

  void activate(polysync::Node & ref);

  void constrain_all();
  void check_and_publish_all();

  void set_steering_safe();
  void set_throttle_safe();
  void set_brake_safe();

  void assure_throttle_safe();
  void assure_gear_safe();

  void request_actuation_status();

  void set_all_safe();
  void set_actuation_mode(bool);

private:
  bool activated = false;

public:
  VehicleState_ vehicle_state;
  PlatformBrakeCommandMessage * brake_message = nullptr;
  PlatformSteeringCommandMessage * steering_message = nullptr;
  PlatformThrottleCommandMessage * throttle_message = nullptr;
  PlatformGearCommandMessage * gear_message = nullptr;
  ParametersMessage * parameters_message = nullptr;


public slots:
  void actuation_mode(bool value) {
    set_actuation_mode(value);
  }

  void steering_mode(unsigned int value) {
    if (steering_message)
    {  
      switch (value)
      {
        case UM_STEERING_MODE_DISABLED: steering_message->setEnabled(false);
                                        break;
        case UM_STEERING_MODE_APP:      steering_message->setEnabled(true);
                                        break;
      }
    }  
  }
  void braking_mode(unsigned int value) {
    if (brake_message)
    {  
      switch (value)
      {
        case UM_BRAKING_MODE_DISABLED: brake_message->setEnabled(false);
                                       break;
        case UM_BRAKING_MODE_APP:      brake_message->setEnabled(true);
                                       break;
      }
    }  
  }  
  void throttle_mode(unsigned int value) {
    if (throttle_message)
    {  
      switch (value)
      {
        case UM_STEERING_MODE_DISABLED: throttle_message->setEnabled(false);
                                        break;
        case UM_STEERING_MODE_APP:      throttle_message->setEnabled(true);
                                        break;
      }
    }  
  }

};

#endif