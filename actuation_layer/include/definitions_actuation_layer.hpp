#ifndef UM_CONTROL_LIMITATIONS
#define UM_CONTROL_LIMITATIONS

#define BRAKE_MAX 0.8

#define THROTTLE_MAX 0.3

#define STEERING_MAX 1

#define STATUS_RATE 10
#endif