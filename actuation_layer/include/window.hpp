// Header for ActuationLayerWindow class
// 
// Authored by Petter Nilsson (pettni@umich.edu), November 2016

#ifndef ACTUATIONLAYERWINDOW_H
#define ACTUATIONLAYERWINDOW_H

#include <iostream>
#include <QMainWindow>
#include <QLabel>

#include "um_general.hpp"

namespace Ui {
class ActuationLayerWindow;
}

class ActuationLayerWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ActuationLayerWindow(QWidget *parent = 0);
    ~ActuationLayerWindow();

private:
    Ui::ActuationLayerWindow *ui;

signals:
    void s_actuation_mode(bool);
    void s_throttle_mode(unsigned int);
    void s_steering_mode(unsigned int);
    void s_braking_mode(unsigned int);

private slots:
    void on_btn_enable_pressed() 
    {
        emit s_actuation_mode(true);
    }
    void on_btn_disable_pressed() 
    {
        emit s_actuation_mode(false);
    }
    void on_btn_enable_all_pressed() 
    {
        emit s_actuation_mode(true);
    }

    void on_btn_th_disabled_pressed() 
    {
        emit s_throttle_mode(UM_THROTTLE_MODE_DISABLED);
    }
    void on_btn_th_app_pressed() 
    {
        emit s_throttle_mode(UM_THROTTLE_MODE_APP);
    }

    void on_btn_st_disabled_pressed() 
    {
        emit s_steering_mode(UM_STEERING_MODE_DISABLED);
    }
    void on_btn_st_app_pressed() 
    {
        emit s_steering_mode(UM_STEERING_MODE_APP);
    }

    void on_btn_br_disabled_pressed() 
    {
        emit s_braking_mode(UM_BRAKING_MODE_DISABLED);
    }
    void on_btn_br_app_pressed() 
    {
        emit s_braking_mode(UM_BRAKING_MODE_APP);
    }

public slots:
    void psync_status(unsigned int s, unsigned long long guid)
    {
        QLabel * label = findChild<QLabel*>("label_psync");
        switch (s)
        {
            case 0: label->setText(QString("Polysync OK with guid 0x%1").arg(guid, 16, 16, QLatin1Char('0')));
                    break;
            case 1: label->setText(QString("Polysync FATAL").arg(s));
                    break;
            case 2: label->setText(QString("Polysync ERROR with guid 0x%1").arg(guid, 16, 16, QLatin1Char('0')));
                    break;
            case 3: label->setText(QString("Polysync WARN with guid 0x%1").arg(guid, 16, 16, QLatin1Char('0')));
                    break;
            case 4: label->setText(QString("Polysync not connected"));
                    break;

        }
    }

    void actuation_status(unsigned long long s)
    {
        QLabel * label = findChild<QLabel*>("label_act");
        switch (s)
        {
            case 0: label->setText(QString("Actuations disabled"));
                    break;
            case 1: label->setText(QString("Actuations enabled"));
                    break;
        }
    }

};
#endif // ACTUATIONLAYERWINDOW_H
