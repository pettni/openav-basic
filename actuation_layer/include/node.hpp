// Header for ActuationLayerNode class
// 
// Authored by Petter Nilsson (pettni@umich.edu), November 2016

#include <PolySyncNode.hpp>
#include <PolySyncDataModel.hpp>
#include <PolySyncDTCException.hpp>

#include <QObject>

#include "commander.hpp"

#ifndef ACT_LAYER_HPP
#define ACT_LAYER_HPP

class ActuationLayerNode : public QObject, public polysync::Node
{
    Q_OBJECT

public:
    ActuationLayerNode(std::shared_ptr<ActuationLayerCommander> cmd) : commander(cmd) {}

    ~ActuationLayerNode() { disconnectPolySync(); }

    virtual void messageEvent(std::shared_ptr< polysync::Message >);

    virtual void initStateEvent();

    void okStateEvent();

    virtual void fatalStateEvent();

    virtual void errorStateEvent();

    virtual void warnStateEvent();

    virtual void releaseStateEvent();


public:
    static constexpr auto NODE_NAME_ = "ActuationLayer";

private:
    ps_msg_type _messageTypeDiagnosticTraceMsg;
    std::shared_ptr<ActuationLayerCommander> commander;
    unsigned int status_counter = 0;


public slots:
    void slot_run() {
        connectPolySync();
    }

signals:
    void psync_status(unsigned int, unsigned long long);
    void actuation_status(bool);
};

#endif