# README #

Module for low-level AV functionality. Contents:

* Safety node ```um_actuation_layer``` that sits between apps and dataspeed interface
* Tests ```um_actuation_test``` and ```um_step_test```

## Requirements ##

* [Polysync](http://polysync.io) (tested with Version 2.0.8-pr.8)
* [qt5](http://qt.io)

## Installation ##

In order to use the custom messages the Polysync data model must be updated. To do so, run the following in the ```openav-basic``` folder:
```
pdm-gen $PSYNC_HOME/modules/dtc/dtc.idl $PSYNC_HOME/modules/sensor/sensor.idl $PSYNC_HOME/modules/navigation/navigation.idl $PSYNC_HOME/modules/control/control.idl ./modules/um_control.idl
cp pdm/*.so $PSYNC_HOME/lib
```
To install in ```$PSYNC_HOME/bin```:
```
mkdir build && cd build
cmake .. && make
make install
```

## Todo list ##

* Add simulation capabilities (low-fidelity model, Carsim)
* Joystick commands?
* More safety features