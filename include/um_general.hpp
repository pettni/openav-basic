#ifndef UM_GENERAL_H
#define UM_GENERAL_H

// Sample time in microseconds
#define SAMPLE_TIME 50000

// Parameter number for actuation parameter
#define UM_PARAM_ACTUATION 4011


// Enumerations for control modes
#define UM_THROTTLE_MODE_DISABLED 0
#define UM_THROTTLE_MODE_APP 1
#define UM_THROTTLE_MODE_CC 2
#define UM_THROTTLE_MODE_ACC 3

#define UM_BRAKING_MODE_DISABLED 0
#define UM_BRAKING_MODE_APP 1

#define UM_STEERING_MODE_DISABLED 0
#define UM_STEERING_MODE_APP 1
#define UM_STEERING_MODE_LK 2

#endif