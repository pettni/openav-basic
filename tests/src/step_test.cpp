// Step tests
//
// Publishes to
//   um_parameter_message
//   um_platform_throttle_command_message
//   um_platform_gear_command_msg
//
// Authored by Petter Nilsson (pettni@umich.edu), November 2016

#include <um_general.hpp>

#include <iostream>
#include <PolySyncNode.hpp>
#include <PolySyncDataModel.hpp>

#include <test_commander.hpp>

// Publish all messages for a given time
void publishForTime(TestCommander & tcommander, double time)
{
  double t = 0;
  while (t < time*1e6) 
  {
    tcommander.publish_all();
    polysync::sleepMicro(SAMPLE_TIME);
    t += SAMPLE_TIME;
  }
}

class StepTestNode : public polysync::Node 
{

private:
  const double level1;
  const double level2;
  const double step_time;

public:

  StepTestNode(double l1, double l2, double s) : level1(l1), level2(l2), step_time(s)
  { 
    setNodeName( "StepTest" );
  }


  ~StepTestNode() 
  {
  }

  void initStateEvent() override
  {   

    TestCommander tcommander(*this);
    publishForTime(tcommander, 1);

    // Publish empty before first test
    publishForTime(tcommander, 3);

    tcommander.brake_message.setBrakeCommandType(BRAKE_COMMAND_PEDAL);
    tcommander.brake_message.setBrakeCommand(0.5);
    tcommander.brake_message.setEnabled(1);
    tcommander.brake_message.setBooEnabled(1);
    publishForTime(tcommander, 2);

    tcommander.gear_message.setGearPosition(GEAR_POSITION_NEUTRAL);
    publishForTime(tcommander, 2);

    tcommander.set_brake_neutral();
    publishForTime(tcommander, 1);

    std::cout << "Entering level 1" << std::endl;
    tcommander.throttle_message.setThrottleCommandType(THROTTLE_COMMAND_PERCENT);
    tcommander.throttle_message.setThrottleCommand(level1);
    tcommander.throttle_message.setEnabled(1);
    publishForTime(tcommander, step_time);

    std::cout << "Entering level 2" << std::endl;
    if (level2 > 0)
    { 
      tcommander.throttle_message.setThrottleCommand(level2);
      tcommander.throttle_message.setEnabled(1);
    }
    else 
    {
      tcommander.set_throttle_neutral();

      tcommander.brake_message.setBrakeCommandType(BRAKE_COMMAND_PERCENT);
      tcommander.brake_message.setBrakeCommand(-level2);
      tcommander.brake_message.setEnabled(1);
      tcommander.brake_message.setBooEnabled(1);
    }

    publishForTime(tcommander, step_time);

    std::cout << "Finished test" << std::endl;
    tcommander.gear_message.setGearPosition(GEAR_POSITION_PARK);

    tcommander.set_neutral();
    publishForTime(tcommander, 2);

    // Exit node
    disconnectPolySync();
  }

};

int main(int argc, char const *argv[])
{
  if (argc < 4)
  {
    std::cout << "Usage: um_step_test level1 level2 step_time" << std::endl; 
    return 0;
  }
  StepTestNode stNode(atof(argv[1]), atof(argv[2]), atof(argv[3]));
  stNode.connectPolySync();
  return 0;
}