#include <um_general.hpp>
#include <test_commander.hpp>

TestCommander::TestCommander(polysync::Node & node_ref) :
                                                  brake_message(node_ref), 
                                                  steering_message(node_ref), 
                                                  throttle_message(node_ref), 
                                                  gear_message(node_ref),
                                                  turn_signal_message(node_ref)
{
  set_neutral();
}

void TestCommander::set_brake_neutral()
{
  brake_message.setBrakeCommandType(BRAKE_COMMAND_INVALID);
  brake_message.setEnabled(0);
  brake_message.setBooEnabled(0);
  brake_message.setBrakeCommand(0.0);
}

void TestCommander::set_steering_neutral()
{
  steering_message.setSteeringCommandKind(STEERING_COMMAND_INVALID);
  steering_message.setEnabled(0);
  steering_message.setSteeringWheelAngle(0.0);
}

void TestCommander::set_throttle_neutral()
{
  throttle_message.setThrottleCommandType(THROTTLE_COMMAND_INVALID);
  throttle_message.setEnabled(0);
  throttle_message.setThrottleCommand(0.0);
}

void TestCommander::set_neutral()
{
  set_brake_neutral();
  set_steering_neutral();
  set_throttle_neutral();
  turn_signal_message.setTurnSignal(PLATFORM_TURN_SIGNAL_INVALID);
  gear_message.setGearPosition(GEAR_POSITION_INVALID);
}    

void TestCommander::publish_all()
{
  int now = polysync::getTimestamp();

  brake_message.setHeaderTimestamp(now); 
  steering_message.setHeaderTimestamp(now);
  throttle_message.setHeaderTimestamp(now);
  turn_signal_message.setHeaderTimestamp(now);
  gear_message.setHeaderTimestamp(now);

  brake_message.publish();
  steering_message.publish();
  throttle_message.publish();
  turn_signal_message.publish();
  gear_message.publish();  
}