// Small open-loop actuation tests for car
//
// Publishes to
//   um_parameter_message
//   um_platform_brake_command_message
//   um_platform_throttle_command_message
//   um_platform_steering_command_message
//   um_platform_gear_command_msg
//   ps_platform_turn_signal_command_msg
//
// Authored by Petter Nilsson (pettni@umich.edu), November 2016

#include <um_general.hpp>

#include <iostream>
#include <PolySyncNode.hpp>
#include <PolySyncDataModel.hpp>

#include <test_commander.hpp>

// Publish all messages for a given time
void publishForTime(TestCommander & tcommander, double time)
{
  double t = 0;
  while (t < time*1e6) 
  {
    tcommander.publish_all();
    polysync::sleepMicro(SAMPLE_TIME);
    t += SAMPLE_TIME;
  }
}

// Shift to drive (ONLY USE WHEN STANDING STILL)
void toDrive(TestCommander & tcommander)
{
  tcommander.brake_message.setBrakeCommandType(BRAKE_COMMAND_PEDAL);
  tcommander.brake_message.setBrakeCommand(0.5);
  tcommander.brake_message.setEnabled(1);
  tcommander.brake_message.setBooEnabled(1);
  publishForTime(tcommander, 2);

  tcommander.gear_message.setGearPosition(GEAR_POSITION_NEUTRAL);
  publishForTime(tcommander, 2);

  tcommander.set_brake_neutral();
  publishForTime(tcommander, 1);
}

// Shift to park (ONLY USE WHEN STANDING STILL)
void toPark(TestCommander & tcommander)
{
  tcommander.brake_message.setBrakeCommandType(BRAKE_COMMAND_PEDAL);
  tcommander.brake_message.setBrakeCommand(0.5);
  tcommander.brake_message.setEnabled(1);
  tcommander.brake_message.setBooEnabled(1);
  publishForTime(tcommander, 2);

  tcommander.gear_message.setGearPosition(GEAR_POSITION_PARK);
  publishForTime(tcommander, 2);

  tcommander.set_brake_neutral();
  publishForTime(tcommander, 1);
}

// Shift to rev (ONLY USE WHEN STANDING STILL)
void toReverse(TestCommander & tcommander)
{
  tcommander.brake_message.setBrakeCommandType(BRAKE_COMMAND_PEDAL);
  tcommander.brake_message.setBrakeCommand(0.5);
  tcommander.brake_message.setEnabled(1);
  tcommander.brake_message.setBooEnabled(1);
  publishForTime(tcommander, 2);

  tcommander.gear_message.setGearPosition(GEAR_POSITION_REVERSE);
  publishForTime(tcommander, 2);

  tcommander.set_brake_neutral();
  publishForTime(tcommander, 1);
}

// Test turn signals
void turnSignalTest(TestCommander & tcommander) 
{
  std::cout << "Initiating turn signal test" << std::endl;

  tcommander.turn_signal_message.setTurnSignal(PLATFORM_TURN_SIGNAL_LEFT);
  publishForTime(tcommander, 4);
  tcommander.turn_signal_message.setTurnSignal(PLATFORM_TURN_SIGNAL_RIGHT);
  publishForTime(tcommander, 4);

  std::cout << "Turn signal test completed" << std::endl << std::endl;

  tcommander.set_neutral();
  publishForTime(tcommander, 3);
}

// Test shifting
void gearTest(TestCommander & tcommander)
{
  std::cout << "Initiating gear test" << std::endl;

  toDrive(tcommander);
  toReverse(tcommander);
  toPark(tcommander);

  std::cout << "Gear test completed" << std::endl << std::endl;

  tcommander.set_neutral();
  publishForTime(tcommander, 3);
}

// Test brakes
void brakeTest(TestCommander & tcommander) 
{
  std::cout << "Initiating brake test" << std::endl;

  tcommander.brake_message.setBrakeCommandType(BRAKE_COMMAND_PEDAL);
  tcommander.brake_message.setBrakeCommand(0.5);
  tcommander.brake_message.setEnabled(1);
  tcommander.brake_message.setBooEnabled(1);
  publishForTime(tcommander, 3);
  
  std::cout << "Brake test completed" << std::endl << std::endl;

  tcommander.set_neutral();
  publishForTime(tcommander, 3);
}

// Test steering
void steeringTest(TestCommander & tcommander)
{
  std::cout << "Initiating steering test" << std::endl;

  tcommander.steering_message.setMaxSteeringWheelRotationRate(1);
  tcommander.steering_message.setSteeringCommandKind(STEERING_COMMAND_ANGLE);
  tcommander.steering_message.setEnabled(1);

  tcommander.steering_message.setSteeringWheelAngle(1.5);
  publishForTime(tcommander, 2);

  tcommander.steering_message.setSteeringWheelAngle(0);
  publishForTime(tcommander, 2);

  tcommander.steering_message.setSteeringWheelAngle(-1.5);
  publishForTime(tcommander, 2);

  std::cout << "Steering test completed" << std::endl << std::endl;

  tcommander.set_neutral();
  publishForTime(tcommander, 3);
}

// Test throttle
void throttleTest(TestCommander & tcommander)
{
  std::cout << "Initiating throttle test" << std::endl;

  tcommander.throttle_message.setThrottleCommandType(THROTTLE_COMMAND_PEDAL);
  tcommander.throttle_message.setThrottleCommand(0.1);
  tcommander.throttle_message.setEnabled(1);
  publishForTime(tcommander, 2);

  std::cout << "Throttle test completed" << std::endl << std::endl;

  tcommander.set_neutral();
  publishForTime(tcommander, 3);
}

// Drive forward, then backward
// This will move the car!!
void startStopTest(TestCommander & tcommander)
{
  std::cout << "Initiating start/stop test" << std::endl;

  toDrive(tcommander);

  tcommander.throttle_message.setThrottleCommandType(THROTTLE_COMMAND_PEDAL);
  tcommander.throttle_message.setThrottleCommand(0.1);
  tcommander.throttle_message.setEnabled(2);
  publishForTime(tcommander, 2);

  tcommander.set_throttle_neutral();

  toReverse(tcommander);

  tcommander.throttle_message.setThrottleCommandType(THROTTLE_COMMAND_PEDAL);
  tcommander.throttle_message.setThrottleCommand(0.1);
  tcommander.throttle_message.setEnabled(2);
  publishForTime(tcommander, 2);

  tcommander.set_throttle_neutral();

  toPark(tcommander);

  std::cout << "Finished start/stop test" << std::endl;

  tcommander.set_neutral();
  publishForTime(tcommander, 3);
}

class ActuationTestNode : public polysync::Node 
{

private:
  const std::string NODE_NAME_ = "um_unit_test";
  const std::vector<int> args;

public:

  ActuationTestNode(const std::vector<int> & v): args(v)
  { 
    setNodeName( NODE_NAME_ );
  }

  void initStateEvent() override
  {   

    TestCommander tcommander(*this);
    publishForTime(tcommander, 1);

    // Publish empty before first test
    publishForTime(tcommander, 3);

    for (int n : args)
    {
      switch(n)
      {
        case 1: turnSignalTest(tcommander);
                break;
        case 2: brakeTest(tcommander);
                break;
        case 3: steeringTest(tcommander);
                break;
        case 4: gearTest(tcommander);
                break;
        case 5: throttleTest(tcommander);
                break;
        case 6: startStopTest(tcommander);
                break;
      }
    }  
    
    // Exit node
    disconnectPolySync();
  }

};

int main(int argc, char const *argv[])
{
  std::vector<int> cmds;
  for (int i = 1; i < argc; ++i)
  { 
    int num = atoi(argv[i]);
    if (num != 0)
    {
      cmds.push_back(num);  
    }
  }

  if (cmds.size() == 0)
  {
    std::cout << "Usage: " << argv[0] <<  " i1 i2 ... in," << std::endl
              << "where i1 ... in are identities of tests." << std::endl 
              << std::endl
              << "1: Turn signal test" << std::endl
              << "2: Brake test" << std::endl
              << "3: Steering test" << std::endl
              << "4: Gear test" << std::endl
              << "5: Throttle test" << std::endl
              << "6: Start/stop test" << std::endl;
    return 0;
  }

  ActuationTestNode utNode(cmds);
  utNode.connectPolySync();
  return 0;
}