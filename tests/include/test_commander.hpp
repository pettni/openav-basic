#include <PolySyncNode.hpp>
#include <PolySyncDataModel.hpp>

#ifndef UM_COMMANDER_H
#define UM_COMMANDER_H

class TestCommander {
public:
  polysync::datamodel::UmPlatformBrakeCommandReqMsg brake_message;
  polysync::datamodel::UmPlatformSteeringCommandReqMsg steering_message;
  polysync::datamodel::UmPlatformThrottleCommandReqMsg throttle_message;
  polysync::datamodel::UmPlatformGearCommandReqMsg gear_message;
  polysync::datamodel::PlatformTurnSignalCommandMessage turn_signal_message;

  TestCommander(polysync::Node &);

  void publish_all();

  void set_steering_neutral();
  void set_throttle_neutral();
  void set_brake_neutral();
  void set_neutral();
};

#endif